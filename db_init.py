import csv
import os
import requests
import time

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from sqlalchemy.orm import sessionmaker

from db_tables import Base, User, Airport

######################################
#   Trying to connect to database    #
######################################
print("ENTERED DB INIT SCRIPT")
database = create_engine("mysql://root:pass@mydb:3306/")

while True:
    try:
        database.connect()
        break
    except OperationalError:
        print("DATABASE IS NOT AVAILABLE NOW")
        time.sleep(1)
        continue
print("DATABASE IS READY FOR CONNECTIONS")


############################################
#   Checking and connect Botbase database  #
############################################
database.execute("create database if not exists Botbase")
database = create_engine("mysql://root:pass@mydb:3306/Botbase?charset=utf8mb4")


######################################
#       Create table airports        #
######################################
if not sqlalchemy.inspect(database).has_table('airports'):
    Airport.metadata.create_all(database)

    Session = sessionmaker(bind=database)
    session = Session()

    codes_file = None
    if not os.path.isfile('airport-codes.csv'):
        codes_file = open('airport-codes.csv', 'w+')
        codes_file.write(requests.get("https://datahub.io/core/airport-codes/r/airport-codes.csv").text)
        codes_file.seek(0, 0)
        print('TEST')
    else:
        codes_file = open('airport-codes.csv', 'r')

    csv_file = csv.DictReader(codes_file)
    for row in csv_file:
        session.add(Airport(row))
    session.commit()

    codes_file.close()


######################################
#       Create table users           #
######################################
if not sqlalchemy.inspect(database).has_table('users'):
    User.metadata.create_all(database)
