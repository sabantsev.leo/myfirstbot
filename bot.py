import telebot
import requests
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from db_tables import Base, User, Airport
from vincenty import vincenty
import time


bot = telebot.TeleBot('5625729265:AAF_l2fyQWh7onL0vWQLezGhjxbi9JG68XY')
api_key = "H_OKca5zrFlPtArat0_ZcLEwUKjCn_dxO-MDBwAeCvQ"  # Microsoft Azure Maps API key
database = create_engine("mysql://root:pass@mydb:3306/Botbase?charset=utf8mb4")


while True:
    try:
        database.connect()
        break
    except OperationalError:
        print("DATABASE IS NOT AVAILABLE NOW")
        time.sleep(3)
        continue
print("BOT IS READY")


Session = sessionmaker(bind=database)
session = Session()


def update_counter(nickname):
    user = session.query(User).filter_by(telegram_name=nickname)
    if user.count() == 0:
        session.add(User(telegram_name=nickname, request_count=0))
        session.commit()
        return

    user.first().request_count += 1
    session.commit()


@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    update_counter(message.from_user.id)
    bot.reply_to(message, "The bot accepts one or two IATA airport codes and returns information about airport" +
                 ", if one code was given, or distance between two airports specified by their codes")
    print("START OR HELP REQUEST")


@bot.message_handler(commands=['test'])
def test_command(message):
    update_counter(message.from_user.id)
    bot.reply_to(message, "Тестовое сообщение")
    print("TEST REQUEST")


@bot.message_handler(commands=['counter'])
def show_counter(message):
    nickname = message.from_user.id
    bot.reply_to(message, "Requests count:" +
                 str(session.query(User).filter_by(telegram_name=nickname).first().request_count))
    print("COUNTER REQUEST")


@bot.message_handler(content_types=['text'])
def echo_all(message):
    update_counter(message.from_user.id)
    db_query = message.text.split()
    query_len = len(db_query)
    if query_len > 2 or query_len < 1:
        bot.reply_to(message, "Please input either one or two airport codes")
        print("WRONG AIRPORT REQUEST")
        return

    db_response = session.query(Airport).filter(Airport.iata_code.in_(db_query))
    airports = []
    for my_code in db_query:
        airport = db_response.filter_by(iata_code=my_code).first()
        if airport is None:
            bot.reply_to(message, f"There isn't an airport with code \"{my_code}\"")
            print("UNKNOWN AIRPORT REQUEST")
            return
        else:
            airports.append(airport)

    reply = ""
    if query_len == 1:
        reply = str(airports[0])
        print("SINGLE AIRPORT REQUEST")
    elif query_len == 2:
        long1, lat1 = airports[0].coordinates.split(', ')
        long2, lat2 = airports[1].coordinates.split(', ')
        coordinates = "{},{}:{},{}".format(lat1, long1, lat2, long2)
        api_query = "https://atlas.microsoft.com/spatial/greatCircleDistance/json?" \
                    "subscription-key={}&api-version=1.0&query={}".format(api_key, coordinates)
        api_response = requests.request("GET", api_query).json()
        reply = f"Distance between {airports[0].name} and {airports[1].name} is: \n"  \
                f"{api_response['result']['distanceInMeters']} metres (microsoft azure maps)\n" \
                f"{1000*vincenty((float(lat1), float(long1)), (float(lat2), float(long2))):.2f} metres (vincenty lib)"
        print("DISTANCE REQUEST")
    bot.reply_to(message, reply)


bot.polling()
