from sqlalchemy import Column, Integer, String, Unicode
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class User(Base):
    __tablename__ = 'users'

    user_id = Column(Integer, primary_key=True)
    telegram_name = Column(String(250))
    request_count = Column(Integer)


class Airport(Base):
    __tablename__ = 'airports'

    ident = Column(String(10), primary_key=True)
    type = Column(String(20))
    name = Column(Unicode(100, collation="utf8_general_ci "))
    elevation_ft = Column(String(10))
    continent = Column(String(10))
    iso_country = Column(String(10))
    iso_region = Column(String(10))
    municipality = Column(Unicode(100, collation="utf8_general_ci "))
    gps_code = Column(String(10))
    iata_code = Column(String(10))
    local_code = Column(String(10))
    coordinates = Column(String(70))

    def __init__(self, init_dict):
        self.ident = init_dict["ident"]
        self.type = init_dict["type"]
        self.name = init_dict["name"]
        self.elevation_ft = init_dict["elevation_ft"]
        self.continent = init_dict["continent"]
        self.iso_country = init_dict["iso_country"]
        self.iso_region = init_dict["iso_region"]
        self.municipality = init_dict["municipality"]
        self.gps_code = init_dict["gps_code"]
        self.iata_code = init_dict["iata_code"]
        self.local_code = init_dict["local_code"]
        self.coordinates = init_dict["coordinates"]

    def __str__(self):
        longitude, latitude = self.coordinates.split(', ')
        return "Airport - %s\n    id: %s\n    code: %s\n    country: %s\n    coordinates: %s  %s\n" \
               % (self.name, self.ident, self.iata_code, self.iso_country, latitude, longitude)

